import os
import time

from dotenv import dotenv_values
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import scripts.mercado_libre as ml
from scripts.mail import Email


def main() -> None:
    start_time = time.time()
    print('Obteniendo variables desde archivo de configuracion...')
    config = dotenv_values(".env")
    # print(config)
    # print(type(config))


    # Creacion de instancia de driver
    # path_driver = config['PATH_DRIVER']
    # # chrome_options = Options()
    # # chrome_options.add_argument("--headless")
    # driver = webdriver.Chrome(executable_path=path_driver)

    # # Carga pagina de mercado libre en el navegador 
    # driver.get(config['URL_SITE'])

    # Buscar producto 
    ruta_salida = os.path.join(config['PATH_REPORT'], 'reporte_mates.xlsx')
    # ml.buscar_producto(driver, 'mate stanley')
    # lista_datos_productos = ml.extraer_datos_productos(driver)
    
    # driver.close()

    # ml.crear_reporte_productos(lista_datos_productos, ruta_salida)


    mail = Email(
    usuario= config['MAIL_USER'],
    clave=config['MAIL_PASS'], 
    servicio=config['MAIL_HOST'], 
    puerto_servicio=int(config['MAIL_PORT']))

    mail.crear_mensaje(
        asunto= config['MAIL_SUBJECT'], 
        ruta_template= config['MAIL_HTML_TEMPLATE'],
        destinatario= config['MAIL_DEST'].split(',')
        )
    
    mail.adjuntar_archivo(archivos=[ruta_salida])
    mail.enviar_correo()
    
    
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    main()
