import time
import traceback

import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from scripts.aplicacion import (enviar_texto, hacer_click, obtener_elementos,
                                obtener_texto)


def buscar_producto(driver, nombre_producto:str) -> None:
    boton_aceptar_cookies = (By.XPATH, '//button[@class="cookie-consent-banner-opt-out__action cookie-consent-banner-opt-out__action--primary cookie-consent-banner-opt-out__action--key-accept"]')
    hacer_click(driver, boton_aceptar_cookies)

    buscador = (By.XPATH, '//*[@id="cb1-edit"]')
    enviar_texto(driver, buscador, nombre_producto)

    boton_buscar = (By.XPATH, '//button[@class="nav-search-btn"]')
    hacer_click(driver, boton_buscar)

def extraer_datos_productos(driver) -> list:
    lista_datos_productos = []
    existe_boton_siguiente = True

    while existe_boton_siguiente: 

        lista_url_productos = obtener_url_productos(driver) 

        print(f'Cantidad de productos -> {len(lista_url_productos)}')
        # Recorrer lista de urls y navegar a pagina que contiene informacion del producot
        for i, url_producto in enumerate(lista_url_productos[:2]):

            driver.get(url_producto)
            
            datos_producto = obtener_datos_producto(driver)
            lista_datos_productos.append(datos_producto)
          

            driver.back()

        # intenta hacer click en boton siguiente 
        try:

            boton_siguiente = (By.XPATH, '//span[contains(text(),"Siguiente")]')
            hacer_click(driver, boton_siguiente)
            print('hace click en siguiente')
        except Exception as e: 
            print('no hace click en siguiente')
            existe_boton_siguiente = False
            traceback.print_exc()
    
    return lista_datos_productos


def obtener_url_productos(driver) -> list:
    lista_url_productos = []

    localizador_lista_productos = (By.XPATH, '//li[@class="ui-search-layout__item shops__layout-item"]')
    lista_productos = obtener_elementos(driver, localizador_lista_productos)
    print(f'Cantidad de productos -> {len(lista_productos)}')
    
    for producto in lista_productos:
        url_producto = producto.find_element_by_xpath('.//a[@class="ui-search-link"]').get_attribute('href')
        lista_url_productos.append(url_producto)

    return lista_url_productos

def obtener_datos_producto(driver) -> list:
    nombre_producto = 'Sin Nombre'
    precio_producto = 'Sin Precio'
    cantidad_de_comentarios = 'Sin Comentarios'
    puntuacion_producto = 'Sin Puntuacion'

    try:
        nombre_producto = obtener_texto(driver, (By.XPATH, '//h1[@class="ui-pdp-title"]'), 3)
        precio_producto = obtener_texto(driver, (By.XPATH, '//div[@class="ui-pdp-price__second-line"]//span[@class="andes-money-amount__fraction"]'), 3)
        cantidad_de_comentarios = obtener_texto(driver, (By.XPATH, '//span[@class="ui-pdp-review__amount"]'), 3)
        puntuacion_producto = obtener_texto(driver, (By.XPATH, '//p[@class="ui-review-capability__rating__average ui-review-capability__rating__average--desktop"]'), 3)
    except Exception as e:
        pass

    return [nombre_producto, precio_producto, cantidad_de_comentarios, puntuacion_producto]

def crear_reporte_productos(lista_datos_productos:list, ruta_salida:str) -> None:
    columnas = ['Nombre', 'Precio', 'Opiniones', 'Calificacion']
    df_mates =  pd.DataFrame(lista_datos_productos, columns=columnas)
    df_mates.to_excel(ruta_salida, sheet_name="Reporte Mates", index=False)

