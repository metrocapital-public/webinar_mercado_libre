import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from jinja2 import Environment, FileSystemLoader, select_autoescape
from premailer import transform


class RutaTemplateError(Exception):
    def __init__(self, ruta_template: str) -> None:
        self.ruta_template = ruta_template
        self.mensaje = 'Ruta entregada no existe o contiene errores de formato, favor validar'
    
    def __str__(self) -> str:
        return f'Error: {self.mensaje} - Ruta: {self.ruta_template}'


class Email: 

    def __init__(self,
        usuario:str = '',
        clave:str = '',
        servicio:str = '',
        puerto_servicio:int = 587) -> None:

        self.usuario = usuario
        self.clave = clave
        self.servicio = servicio
        self.puerto_servicio = puerto_servicio
        self._mensaje = MIMEMultipart('alternative') 

    def _renderizar_template(self, ruta_template:str, datos:dict) -> str:
        if not os.path.exists(ruta_template):
            raise RutaTemplateError(ruta_template)
     
        
        directorio = os.path.dirname(ruta_template)
        archivo = os.path.basename(ruta_template)
            
        env = Environment(
            loader=FileSystemLoader(directorio),
            autoescape=select_autoescape(['html']))
        texto_template = env.get_template(archivo)
        template_renderizado = texto_template.render(datos=datos)
        return template_renderizado


    def crear_mensaje(self, 
                    asunto:str, 
                    ruta_template:str,
                    destinatario:list[str],
                    cc:list[str] = [''],
                    cco:list[str] = [''],
                    datos:dict = {}) -> None:
        self._mensaje = MIMEMultipart('mixed') 
        self._mensaje['From']    = self.usuario
        self._mensaje['Subject'] = asunto
        self._mensaje['To']      = ', '.join(destinatario)
        self._mensaje['Cc']      = ', '.join(cc)
        self._mensaje['Bcc']     = ', '.join(cco)
        
        
        html_renderizado = self._renderizar_template(ruta_template, datos)
        self._mensaje.attach(MIMEText(html_renderizado, "html"))

    
    def agregar_imagen(self, ruta_imagen:str) -> None:
        with open(ruta_imagen, 'rb') as img:
            msg_imagen = MIMEImage(img.read())
        
        msg_imagen.add_header('Content-ID', '<image_id_1>')
        self._mensaje.attach(msg_imagen)


    def adjuntar_archivo(self, archivos:list) -> None:
        for archivo in archivos:
            nombre_archivo = os.path.basename(archivo)
            with open (archivo, 'rb') as f:
                adjunto = MIMEBase('multipart', 'encrypted')
                adjunto.set_payload(f.read())
                encoders.encode_base64(adjunto)
                adjunto.add_header('Content-Disposition', 'attachment',filename=nombre_archivo)
            self._mensaje.attach(adjunto)

            
    def enviar_correo (self) -> None:

        with smtplib.SMTP(self.servicio, self.puerto_servicio) as smtp:
            smtp.starttls()
            smtp.login(self.usuario, self.clave)
            smtp.send_message(self._mensaje)

