import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

USER = ''
PASS = ''
HOST = 'smtp.gmail.com'
PORT = 587
DEST = ''
ARCHIVO = r'C:\Users\Alejandro del Pino\Documents\MetroCapital\Webinars\00 - Mercado Libre\Codigo\reporte_mates.xlsx'
TEST_HOST = 'localhost'
TEST_PORT = 1025

mensaje = MIMEMultipart()
mensaje['Subject'] = 'Correo prueba webinar'
mensaje['From'] = USER
mensaje['To'] = DEST
mensaje.attach(MIMEText('<h1>Holi esto es un correo de prueba para mi webinar!</h1>', 'html'))

with open (ARCHIVO, 'rb') as f:
    # Creating a MIME object with the file content.
    adjunto = MIMEBase('multipart', 'encrypted')
    adjunto.set_payload(f.read())
    encoders.encode_base64(adjunto)
    adjunto.add_header('Content-Disposition', 'attachment',filename='hola.xlsx')
    mensaje.attach(adjunto)

with smtplib.SMTP(HOST, PORT) as smtp:
    smtp.starttls()
    smtp.login(user=USER, password=PASS)
    smtp.send_message(mensaje)




