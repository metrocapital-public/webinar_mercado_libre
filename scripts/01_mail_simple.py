import smtplib

# python -m smtpd -n -c DebuggingServer localhost:1025

USER = ''
PASS = ''

HOST = 'smtp.gmail.com'
PORT = 587
DEST = 'webinar.mercado.libre@gmail.com'

TEST_HOST = 'localhost'
TEST_PORT = 1025

with smtplib.SMTP(HOST, PORT) as smtp:
    smtp.starttls()
    smtp.login(user=USER, password=PASS)

    asunto = 'Correo prueba webinar'
    cuerpo = 'Holi esto es un correo de prueba para mi webinar!'

    mensaje = f'Subject: {asunto}\n{asunto}'

    smtp.sendmail(USER, DEST, mensaje)




